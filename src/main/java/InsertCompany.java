
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author USER01
 */
public class InsertCompany {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO COMPANY(ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(1,'Paul', 32, 'California', 20000.00);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO COMPANY(ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(2,'Allen', 25, 'Texas', 15000.00);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO COMPANY(ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(3,'Teddy', 23, 'Norway', 20000.00);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO COMPANY(ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(4,'Mark', 25, 'Rich-Mond', 65000.00);";
            stmt.executeUpdate(sql);
            conn.commit();
            conn.close();
            stmt.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No Library org.sqlite.JDBC");
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
        }
    }
}
