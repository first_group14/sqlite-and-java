
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author USER01
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (username, password)"
                    + "VALUES('user3', 'password');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username, password)"
                    + "VALUES('Karina', 'password');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username, password)"
                    + "VALUES('Winter', 'password');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username, password)"
                    + "VALUES('Miyeon', 'password');";
            stmt.executeUpdate(sql);

            conn.commit();
            conn.close();
            stmt.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No Library org.sqlite.JDBC");
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
        }
    }
}
