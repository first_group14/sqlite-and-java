
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author USER01
 */
public class DeleteUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            //Delete
            stmt.executeUpdate("DELETE FROM user WHERE username = 'user3'");
            conn.commit();
            //Select
            ResultSet rs = stmt.executeQuery("SELECT * FROM user");
            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");

                System.out.println("ID = " + id);
                System.out.println("USERNAME = " + username);
                System.out.println("PASSWORD = " + password);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No Library org.sqlite.JDBC");
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
        }
    }
}
