
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author USER01
 */
public class DeleteCompany {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            //Delete
            stmt.executeUpdate("DELETE FROM COMPANY WHERE ID = 2");
            conn.commit();
            //Select
            ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY");
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String address = rs.getString("address");
                float salary = rs.getFloat("salary");
                System.out.println("ID = "+id);
                System.out.println("NAME = "+name);
                System.out.println("AGE = "+age);
                System.out.println("ADDRESS = "+address);
                System.out.println("SALARY = "+salary);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No Library org.sqlite.JDBC");
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
        }
    }
}
